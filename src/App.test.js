import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app/store';
import App from './App';
import userEvent from '@testing-library/user-event'

test('renders static data', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(getByText(/Stretch Tracker/i)).toBeInTheDocument();
  expect(getByText(/track daily stretches/i)).toBeInTheDocument();
});

test('renders categories', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(getByText(/Hands/i)).toBeInTheDocument();
  expect(getByText(/Arms/i)).toBeInTheDocument();
  expect(getByText(/Shoulders/i)).toBeInTheDocument();


  userEvent.click(screen.getAllByRole('button')[0])
  expect(screen.getAllByRole('button')[0]).toHaveTextContent('Done ✅')
});

import React from 'react';
import { TaskList } from './features/tasklist/TaskList';
import './App.css';

function App() {
  return (
    <div className="App">
      <header>Stretch Tracker</header>
      <div className="desc">
        This application allows to track daily stretches. Since it has no backend, the page must stay opened during the day but can still be useful.
      </div>
      <TaskList category="Hands" />
      <TaskList category="Arms" />
      <TaskList category="Shoulders" />
    </div>
  );
}

export default App;

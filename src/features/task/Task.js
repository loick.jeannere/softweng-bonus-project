import { useDispatch } from 'react-redux';
import {
    toggleDoneByIndex
} from './../tasklist/taskListSlice'

import styles from './Task.module.css';

export function Task(state, ownProps) {
    const dispatch = useDispatch();

    let button;
    if (state.done) {
        button = <button className={styles.undoButton} onClick={() => dispatch(toggleDoneByIndex(state.index))}>Done ✅</button>
    }
    else {
        button = <button className={styles.button} onClick={() => dispatch(toggleDoneByIndex(state.index))}>Stretch done 💪</button>
    }

    return (
        <div className={styles.card}>
            {button} <span className={styles.title}>{state.title}</span>
        </div>
    );
}

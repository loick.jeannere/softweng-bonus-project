import { createSlice } from '@reduxjs/toolkit';


const initialState = [
    { category: "Hands", title: "Stretch left hand", completed: false },
    { category: "Hands", title: "Stretch right hand", completed: false },
    { category: "Hands", title: "Stretch fingers", completed: false },
    { category: "Shoulders", title: "Stretch left shoulder", completed: false },
    { category: "Shoulders", title: "Stretch right shoulder", completed: false },
    { category: "Shoulders", title: "Stretch both arm above", completed: false },
    { category: "Arms", title: "Stretch left forearm", completed: false },
    { category: "Arms", title: "Stretch right forearm", completed: false },
    { category: "Arms", title: "Stretch left upper arm", completed: false },
    { category: "Arms", title: "Stretch right upper arm", completed: false }
];

for (let i = 0; i < initialState.length; i++) {
    initialState[i].index = i;
}

export const selectTasks = (state) => state.tasks;

export const taskListSlice = createSlice({
    name: 'tasks',
    initialState,
    reducers: {
        toggleDoneByIndex: (state, action) => {
            state[action.payload].completed = !state[action.payload].completed;
        },
        resetDoneByCategory: (state, action) => {
            for (const task of state) {
                if (task.category === action.payload) {
                    task.completed = false;
                }
            }
        }
    }
});

export default taskListSlice.reducer;
export const { toggleDoneByIndex, resetDoneByCategory } = taskListSlice.actions;

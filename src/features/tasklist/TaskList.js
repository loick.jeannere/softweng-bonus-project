import { React } from 'react';
import { Task } from '../task/Task';
import { useSelector, useDispatch } from 'react-redux';
import styles from './Tasklist.module.css';

import {
  resetDoneByCategory
} from './taskListSlice'

const selectTasks = state => state.tasks;

export function TaskList(state, ownProps) {
  const tasks = useSelector(selectTasks);
  const dispatch = useDispatch();

  const associatedTasks = tasks.filter(task => task.category === state.category);
  const completedTasks = associatedTasks.reduce((count, task) => count + (task.completed ? 1 : 0), 0)

  return (
    <div className={styles.card}>
      <div className={styles.title}>{state.category} - {completedTasks} / {associatedTasks.length}</div>
      <hr />
      {
        completedTasks !== associatedTasks.length ?
          associatedTasks.map(task => {
            return <Task index={task.index} key={task.index} title={task.title} done={task.completed}></Task>
          }) : (
            <div>
              <p>All stretches completed</p>
              <button className={styles.button} onClick={() => dispatch(resetDoneByCategory(state.category))}>Reset "{state.category}" stretches</button>
            </div>
          )
      }
    </div>)
}

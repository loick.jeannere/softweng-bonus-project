import taskListReducer, {
    toggleDoneByIndex,
    resetDoneByCategory
} from './taskListSlice';

describe('taskList reducer', () => {
    const initialState = [
        { category: "Hands", title: "Stretch left hand", completed: false },
        { category: "Hands", title: "Stretch right hand", completed: false },
        { category: "Shoulders", title: "Stretch right shoulder", completed: false },
        { category: "Shoulders", title: "Stretch both arm above", completed: false }
    ];

    it('should handle toggling completed', () => {
        let state = taskListReducer(initialState, toggleDoneByIndex(0));

        expect(state[0].completed).toEqual(true);
        expect(state[1].completed).toEqual(false);
        expect(state[2].completed).toEqual(false);
        expect(state[3].completed).toEqual(false);

        state = taskListReducer(state, toggleDoneByIndex(0));

        expect(state[0].completed).toEqual(false);
        expect(state[1].completed).toEqual(false);
        expect(state[2].completed).toEqual(false);
        expect(state[3].completed).toEqual(false);
    });

    it('should handle reset category', () => {
        let state = taskListReducer(initialState, toggleDoneByIndex(0));

        state = taskListReducer(state, toggleDoneByIndex(2));

        expect(state[0].completed).toEqual(true);
        expect(state[1].completed).toEqual(false);
        expect(state[2].completed).toEqual(true);
        expect(state[3].completed).toEqual(false);

        state = taskListReducer(state, resetDoneByCategory("Hands"));
        expect(state[0].completed).toEqual(false);
        expect(state[1].completed).toEqual(false);
        expect(state[2].completed).toEqual(true);
        expect(state[3].completed).toEqual(false);
    });

});
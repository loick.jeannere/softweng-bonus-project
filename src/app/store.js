import { configureStore } from '@reduxjs/toolkit';
import tasksReducer from '../features/tasklist/taskListSlice';

export const store = configureStore({
  reducer: {
    tasks: tasksReducer,
  },
});

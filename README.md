# About

Simple application to track which daily stretches have been done (this is an issue I actually have and that can be solved by this website). 

- Uses React and Redux
  - Has parent/child components: stretch list containing stretches
  - Handles user interaction with Redux (completing a stretch task)
- Has UI and unit tests
  - Continuous integration is setup on GitLab
  - Tests are triggered on push
- Can be run inside a docker container

# Running the application

Clone the project: `git clone https://gitlab.forge.hefr.ch/loick.jeannere/softweng-bonus-project.git`

## Setup (without docker)

Setting up without docker allows to run tests manually.
Dependencies required: NodeJS and npm (tested on node v12.18.4)

```
npm install
```

## Run

```
npm start
```

## Test

```
npm test
```

## Setup (with docker)

1. Build container `docker build . -t stretch-checker`
2. Run container `docker run -p 3000:3000 stretch-checker`

Website is then accessible on localhost:3000

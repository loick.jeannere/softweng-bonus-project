# Dockerfile based on https://mherman.org/blog/dockerizing-a-react-app/
FROM node:13.12.0-alpine

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent

COPY . ./

# TODO: This should probably use `npm run build` and setup a real webserver, but for the purpose of this exercice this is as far as the project will go
CMD ["npm", "start"]
